package fr.klanik.buzatu.demo.phonetchat;

/**
 * Class representing and SMS
 */
public class Sms {

    private String fromNumber;

    private String message;

    /**
     * Constructor
     * 
     * @param fromNumber
     * @param message
     */
    public Sms(String fromNumber, String message) {
        this.fromNumber = fromNumber;
        this.message = message;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
