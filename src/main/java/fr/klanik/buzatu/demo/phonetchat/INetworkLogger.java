package fr.klanik.buzatu.demo.phonetchat;

public interface INetworkLogger {

    void logEvent(String number, Sms message);
}
