package fr.klanik.buzatu.demo.phonetchat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Network
{
    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");

    public Network() {
        numbers = new ArrayList<>();
        messages = new HashMap<>();

        logger = (number, sms) -> System.out.println(dateFormatter.format(LocalDateTime.now()) + " : N° " + sms.getFromNumber() + " sent \"" + sms.getMessage() + " to n°" + number);
    }

    public String get(String number) {
        String res = null;

//        for (String n : numbers) {
//            if (number == n) {
//                res = n;
//            }
//        }

        try {
            res = numbers.stream().filter(num -> num == number).findFirst().get();
        } catch (Exception ex) {}

        return res;
    }

    public void addNumber(String number) {
        numbers.add(number);
    }

    public void remove(String number) {
        numbers.remove(number);
    }

    public void addMessage(String number, Sms message) {
        List<Sms> list = new ArrayList<>();
        list.add(message);

        messages.remove(number);

        messages.put(number, list);

        logger.logEvent(number, message);
    }

    public void removeMessage(String number, Sms message) {
        List<Sms> list = messages.get(number);

        list.remove(message);

        messages.remove(number);
        messages.put(number, list);
    }

    public List<Sms> getMessages(String number) throws Exception {
        if (get(number) == null) {
            throw new Exception("The phone number " + number + " is not registered");
        }

        List<Sms> m = null;

        m = messages.get(number);

        return m;
    }

    private List<String> numbers;

    private Map<String, List<Sms>> messages;

    private INetworkLogger logger;
}
