package fr.klanik.buzatu.demo.phonetchat;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a phone
 */
public abstract class Phone extends JWindow implements WindowListener {

    // GUI stuff

    JFrame mainFrame;
    JDesktopPane mainPlan;

    JLabel numberLabel;

    JLabel smsToLabel;
    JTextField smsToText;

    JLabel smsLabel;
    JTextField smsText;

    JButton sendButton;
    JButton powerButton;

    JTextArea textArea;


    // Attributes
    protected Network network;

    protected String number;

    protected Os operatingSystem;

    protected String constructor;

    protected String model;

    protected boolean powerOn;

    private Thread thread;

    private List<String> inbox;



    /**
     * Phone constructor
     *
     * @param number
     * @param operatingSystem
     * @param constructor
     * @param model
     */
    public Phone(String number, Os operatingSystem, String constructor, String model, Network network) {
        this.number = number;
        this.operatingSystem = operatingSystem;
        this.constructor = constructor;
        this.model = model;
        this.network = network;
        inbox = new ArrayList<>();

        // GUI initialisation
        initGraphicInterface();
    }

    /**
     * Thread run method that listens all the time for messages while the phone is powered on
     */
    public void run() {

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                // Infinite loop
                while (true) {

                    // While the phone is on we check the messages
                    if (powerOn) {
                        try {
                            // Getting the messages from the network
                            List<Sms> messages = network.getMessages(number);

                            // For each message we display it and the erase it from the network
                            if (messages != null) {
                                for (int i = 0; i < messages.size(); i++) {
                                    displayIncomingMessage(messages.get(i));
                                    network.removeMessage(number, messages.get(i));
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });

        thread.start();
    }

    /**
     * Method to send an sms to another phone
     * @param message
     * @param number
     */
    public abstract void sendSms(String message, String number) throws Exception;

    /**
     * Method to display an incoming phone
     *
     * @param message
     */
    public abstract void displayIncomingMessage(Sms message);


    /**
     * Initializing the GUI
     */
    protected abstract void initGraphicInterface();








    /**
     * The action for the send button
     */
    protected void sendSmsAction() {
        try {
            sendSms(smsText.getText(), smsToText.getText());
            smsText.setText("");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * The action for the power button
     */
    protected void togglePower() {
        powerOn = !powerOn;

        if (powerOn) {
            run();
            network.addNumber(number);
            powerButton.setText("Power off");
            textArea.setText("Phone powered on.");
        } else {
            thread.stop();
            network.remove(number);
            powerButton.setText("Power on");
            textArea.setText("Phone powered off.");
        }
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Os getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(Os operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getConstructor() {
        return constructor;
    }

    public void setConstructor(String constructor) {
        this.constructor = constructor;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean isPowerOn() {
        return powerOn;
    }

    public void setPowerOn(boolean powerOn) {
        this.powerOn = powerOn;
    }

    public List<String> getInbox() {
        return inbox;
    }

    public void setInbox(List<String> inbox) {
        this.inbox = inbox;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    /* GUI Methods */

    @Override
    public void windowClosing(WindowEvent arg0) {
        this.setVisible(false);
        this.dispose();
        this.setPowerOn(false);
        System.exit(0);
    }

    @Override
    public void windowOpened(WindowEvent arg0) {
        System.out.println(constructor + " - " + model  + " started.");
    }

    @Override
    public void windowActivated(WindowEvent arg0) {
    }

    @Override
    public void windowClosed(WindowEvent arg0) {
    }

    @Override
    public void windowDeactivated(WindowEvent arg0) {
    }

    @Override
    public void windowDeiconified(WindowEvent arg0) {
    }

    @Override
    public void windowIconified(WindowEvent arg0) {
    }

}
