package fr.klanik.buzatu.demo.phonetchat;

import java.util.HashSet;
import java.util.Set;

/**
 * Class for learning and teaching purposes
 *
 * The application creates severals phones and a network with who the phone will communicate via SMS.
 * Each phone has a GUI which can control it.
 *
 */
public class PhoneTchat {
    // The network for the phones
    public static Network network;

    static {
        network = new Network();
    }

    public PhoneTchat() {
    }

    /**
     * Demo play test to play around and test the phones
     */
    public void startPlayTest() {
        SmartPhone p30Pro = new SmartPhone("0636547895", Os.ANDROID, "Huawei", "P30 Pro");
        CellularPhone nokia3310 = new CellularPhone("0612587913", Os.OTHER, "Nokia", "3310");
    }

    /**
     *      Case 1 :
     *
     *      Create a new play test method with 2 new Phones :
     *
     *      - A Samsung GalaxyS 10 on Android, the phone number is irrelevant
     *      - A Apple Iphone10 on IOS, the phone number is irrelevant
     */


    /**
     *      Case 2 :
     *
     *      Use any of those play tests to do the following
     *
     *      - Power on just one phone and send the other phone 2 sms
     *      - Power on the second phone
     *
     *      Question :
     *
     *          => Why the second phone receives only the last sms ?
     *             Try to fix it.
     */


    /**
     *      Case 3 :
     *
     *      Use any of those play tests to do the following
     *
     *      - Send an sms to a a phone number that doesn't exist
     *
     *      Question :
     *
     *          => What's happening ?
     *             Try to help the user to know more about the situation
     */


    /**
     *      Case 4 :
     *
     *     In the network class write another logger that instead of just writing it to sdt, writes it into a local file.
     *
     */


    /**
     *      Testing the equality between 2 Sms.
     *
     *      Question
     *
     *          => Why the following result is false ?
     *          => Can you correct this ?
     *
     */

    public static void testEquality() {
        String message = "It's the same message";
        String number = "0698147563";

        Sms first = new Sms(number, message);
        Sms second = new Sms(number, message);

        System.out.println(first.equals(second));
    }

    /**
     *      Testing the uniqueness of a set with sms's
     *
     *      Question :
     *
     *          => Why the following set size is 2 ?
     *          => Can you correct this ?
     */
    public static void testSet() {
        String message = "It's the same message";
        String number = "0698147563";

        Sms first = new Sms(number, message);
        Sms second = new Sms(number, message);

        Set<Sms> smsUniqueSet = new HashSet<>();

        smsUniqueSet.add(first);
        smsUniqueSet.add(second);

        System.out.println("Size of the set : " + smsUniqueSet.size());
    }

    /**
     *      What happens here ?
     */
    public static void what() {
        int number = 2147483647;

        System.out.println(number + 33);
    }

    public static void main( String[] args ) {
        PhoneTchat phoneTchat = new PhoneTchat();

        phoneTchat.startPlayTest();

//        PhoneTchat.testEquality();

//        PhoneTchat.testSet();

//        PhoneTchat.what();
    }
}
