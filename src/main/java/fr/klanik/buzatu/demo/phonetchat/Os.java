package fr.klanik.buzatu.demo.phonetchat;

public enum Os {
    IOS,
    ANDROID,
    WINDOWS_PHONE,
    OTHER;
}
