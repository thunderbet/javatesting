package fr.klanik.buzatu.demo.phonetchat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Class representing a smartphone, extends the Phone class.
 *
 */
public class SmartPhone extends Phone {

    /**
     * Constructor
     *
     * @param number
     * @param operatingSystem
     * @param constructor
     * @param model
     */
    public SmartPhone(String number, Os operatingSystem, String constructor, String model) {
        super(number, operatingSystem, constructor, model, PhoneTchat.network);
    }

    @Override
    public void sendSms(String message, String number) throws Exception {
        if (isPowerOn()) {
            network.addMessage(number, new Sms(getNumber(), message));
        } else {
            throw new Exception("Cannot send a message while the phone is not turned on");
        }
    }

    @Override
    public void displayIncomingMessage(Sms sms) {
        this.textArea.setText(sms.getFromNumber() + " : \r\n " + sms.getMessage());
        System.out.println("Received message from phone " + sms.getFromNumber() + " : " + sms.getMessage());
    }

    /**
     * Initializing the GUI
     */
    @Override
    protected void initGraphicInterface() {
        mainFrame = new JFrame();
        mainFrame.setTitle(this.constructor + " - " + this.model);
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
        mainFrame.setSize(250, 500);

        int xPosition = constructor.length() * 100;
        int yPosition = model.length() * 30;

        mainFrame.setLocation(xPosition, yPosition);
        mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(this);

        // Main plan for components
        mainPlan = new JDesktopPane();
        mainPlan.setBackground(Color.BLACK);
        Container mainFrameContentPane = mainFrame.getContentPane();
        mainFrameContentPane.setLayout(new BorderLayout());
        mainFrameContentPane.add(mainPlan, BorderLayout.CENTER);

        // Components
        numberLabel = new JLabel("N° : " + this.number);
        numberLabel.setForeground(Color.WHITE);
        numberLabel.setBounds(10,5, 100, 25);
        mainPlan.add(numberLabel, JLayeredPane.DEFAULT_LAYER);


        textArea = new JTextArea();
        textArea.setForeground(Color.BLACK);
        textArea.setBackground(Color.WHITE);
        textArea.setBounds(5, 50, 235, 300);
        mainPlan.add(textArea, JLayeredPane.DEFAULT_LAYER);

        smsToLabel = new JLabel("To N° : ");
        smsToLabel.setForeground(Color.WHITE);
        smsToLabel.setBounds(5, 365, 45, 25);
        mainPlan.add(smsToLabel, JLayeredPane.DEFAULT_LAYER);

        smsToText = new JTextField("Phone number");
        smsToText.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (smsToText.getText().equals("Phone number") ) {
                    smsToText.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (smsToText.getText().isEmpty()) {
                    smsToText.setText("Phone number");
                }
            }
        });
        smsToText.setForeground(Color.BLACK);
        smsToText.setBackground(Color.WHITE);
        smsToText.setBounds(45, 365, 195, 25);
        mainPlan.add(smsToText, JLayeredPane.DEFAULT_LAYER);

        smsLabel = new JLabel("Sms : ");
        smsLabel.setForeground(Color.WHITE);
        smsLabel.setBounds(5, 400, 35, 25);
        mainPlan.add(smsLabel, JLayeredPane.DEFAULT_LAYER);

        smsText = new JTextField("Your sms");
        smsText.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (smsText.getText().equals("Your sms") ) {
                    smsText.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (smsText.getText().isEmpty()) {
                    smsText.setText("Your sms");
                }
            }
        });
        smsText.setForeground(Color.BLACK);
        smsText.setBackground(Color.WHITE);
        smsText.setBounds(45, 400, 195, 25);
        mainPlan.add(smsText, JLayeredPane.DEFAULT_LAYER);

        sendButton = new JButton("Send");
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendSmsAction();
            }
        });
        sendButton.setBounds(170, 445, 70, 25);
        mainPlan.add(sendButton, JLayeredPane.DEFAULT_LAYER);

        powerButton = new JButton("Power On");

        powerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                togglePower();
            }
        });
        powerButton.setBounds(150,5, 90, 25);
        mainPlan.add(powerButton, JLayeredPane.DEFAULT_LAYER);
    }

}
