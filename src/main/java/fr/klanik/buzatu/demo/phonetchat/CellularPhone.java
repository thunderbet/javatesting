package fr.klanik.buzatu.demo.phonetchat;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;

public class CellularPhone extends Phone {

    JLabel digit0;
    JLabel digit1;
    JLabel digit2;
    JLabel digit3;
    JLabel digit4;
    JLabel digit5;
    JLabel digit6;
    JLabel digit7;
    JLabel digit8;
    JLabel digit9;
    JLabel digitPlus;
    JLabel digitStar;

    /**
     * Constructor
     *
     * @param number
     * @param operatingSystem
     * @param constructor
     * @param model
     */
    public CellularPhone(String number, Os operatingSystem, String constructor, String model) {
        super(number, operatingSystem, constructor, model, PhoneTchat.network);

        initExtraUI();
    }

    @Override
    public void sendSms(String message, String number) throws Exception {
        if (isPowerOn()) {
            network.addMessage(number, new Sms(getNumber(), message));
        } else {
            throw new Exception("Cannot send a message while the phone is not turned on");
        }
    }

    @Override
    public void displayIncomingMessage(Sms sms) {
        this.textArea.setText(sms.getFromNumber() + " : \r\n " + sms.getMessage());
        System.out.println("Received message from phone " + sms.getFromNumber() + " : " + sms.getMessage());
    }


    /**
     * Initializing the GUI
     */
    @Override
    protected void initGraphicInterface() {
        mainFrame = new JFrame();
        mainFrame.setTitle(this.constructor + " - " + this.model);
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
        mainFrame.setSize(250, 500);

        int xPosition = constructor.length() * 100;
        int yPosition = model.length() * 30;

        mainFrame.setLocation(xPosition, yPosition);
        mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(this);

        // Main plan for components
        mainPlan = new JDesktopPane();
        mainPlan.setBackground(Color.BLACK);
        Container mainFrameContentPane = mainFrame.getContentPane();
        mainFrameContentPane.setLayout(new BorderLayout());
        mainFrameContentPane.add(mainPlan, BorderLayout.CENTER);

        // Components
        numberLabel = new JLabel("N° : " + this.number);
        numberLabel.setForeground(Color.WHITE);
        numberLabel.setBounds(10,5, 100, 25);
        mainPlan.add(numberLabel, JLayeredPane.DEFAULT_LAYER);


        textArea = new JTextArea();
        textArea.setForeground(Color.BLACK);
        textArea.setBackground(Color.WHITE);
        textArea.setBounds(5, 50, 235, 50);
        mainPlan.add(textArea, JLayeredPane.DEFAULT_LAYER);

        smsToLabel = new JLabel("To N° : ");
        smsToLabel.setForeground(Color.WHITE);
        smsToLabel.setBounds(5, 365, 45, 25);
        mainPlan.add(smsToLabel, JLayeredPane.DEFAULT_LAYER);

        smsToText = new JTextField("");
        smsToText.setForeground(Color.BLACK);
        smsToText.setBackground(Color.WHITE);
        smsToText.setBounds(45, 365, 195, 25);
        mainPlan.add(smsToText, JLayeredPane.DEFAULT_LAYER);

        smsLabel = new JLabel("Sms : ");
        smsLabel.setForeground(Color.WHITE);
        smsLabel.setBounds(5, 400, 35, 25);
        mainPlan.add(smsLabel, JLayeredPane.DEFAULT_LAYER);

        smsText = new JTextField("");
        smsText.setForeground(Color.BLACK);
        smsText.setBackground(Color.WHITE);
        smsText.setBounds(45, 400, 195, 25);
        mainPlan.add(smsText, JLayeredPane.DEFAULT_LAYER);

        sendButton = new JButton("Send");
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendSmsAction();
            }
        });
        sendButton.setBounds(170, 445, 70, 25);
        mainPlan.add(sendButton, JLayeredPane.DEFAULT_LAYER);

        powerButton = new JButton("Power On");

        powerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                togglePower();
            }
        });
        powerButton.setBounds(150,5, 90, 25);
        mainPlan.add(powerButton, JLayeredPane.DEFAULT_LAYER);
    }

    private void initExtraUI() {
        Border border = BorderFactory.createLineBorder(Color.white);

        digit1 = new JLabel("1");
        addListener(digit1,"1");
        digit1.setBounds(10, 105, 60, 60);
        digit1.setBorder(border);
        digit1.setForeground(Color.white);
        digit1.setFont(digit1.getFont().deriveFont(50f));
        digit1.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit1, JLayeredPane.DEFAULT_LAYER);

        digit2 = new JLabel("2");
        addListener(digit2,"2");
        digit2.setBounds(85, 105, 60, 60);
        digit2.setBorder(border);
        digit2.setForeground(Color.white);
        digit2.setFont(digit1.getFont().deriveFont(50f));
        digit2.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit2, JLayeredPane.DEFAULT_LAYER);

        digit3 = new JLabel("3");
        addListener(digit3,"3");
        digit3.setBounds(160, 105, 60, 60);
        digit3.setBorder(border);
        digit3.setForeground(Color.white);
        digit3.setFont(digit1.getFont().deriveFont(50f));
        digit3.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit3, JLayeredPane.DEFAULT_LAYER);

        digit4 = new JLabel("4");
        addListener(digit4,"4");
        digit4.setBounds(10, 170, 60, 60);
        digit4.setBorder(border);
        digit4.setForeground(Color.white);
        digit4.setFont(digit1.getFont().deriveFont(50f));
        digit4.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit4, JLayeredPane.DEFAULT_LAYER);

        digit5 = new JLabel("5");
        addListener(digit5,"5");
        digit5.setBounds(85, 170, 60, 60);
        digit5.setBorder(border);
        digit5.setForeground(Color.white);
        digit5.setFont(digit1.getFont().deriveFont(50f));
        digit5.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit5, JLayeredPane.DEFAULT_LAYER);

        digit6 = new JLabel("6");
        addListener(digit6,"6");
        digit6.setBounds(160, 170, 60, 60);
        digit6.setBorder(border);
        digit6.setForeground(Color.white);
        digit6.setFont(digit1.getFont().deriveFont(50f));
        digit6.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit6, JLayeredPane.DEFAULT_LAYER);

        digit7 = new JLabel("7");
        addListener(digit7,"7");
        digit7.setBounds(10, 235, 60, 60);
        digit7.setBorder(border);
        digit7.setForeground(Color.white);
        digit7.setFont(digit1.getFont().deriveFont(50f));
        digit7.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit7, JLayeredPane.DEFAULT_LAYER);

        digit8 = new JLabel("8");
        addListener(digit8,"8");
        digit8.setBounds(85, 235, 60, 60);
        digit8.setBorder(border);
        digit8.setForeground(Color.white);
        digit8.setFont(digit1.getFont().deriveFont(50f));
        digit8.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit8, JLayeredPane.DEFAULT_LAYER);

        digit9 = new JLabel("9");
        addListener(digit9,"9");
        digit9.setBounds(160, 235, 60, 60);
        digit9.setBorder(border);
        digit9.setForeground(Color.white);
        digit9.setFont(digit1.getFont().deriveFont(50f));
        digit9.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit9, JLayeredPane.DEFAULT_LAYER);

        digitStar = new JLabel("*");
        addListener(digitStar,"*");
        digitStar.setBounds(10, 300, 60, 60);
        digitStar.setBorder(border);
        digitStar.setForeground(Color.white);
        digitStar.setFont(digit1.getFont().deriveFont(50f));
        digitStar.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digitStar, JLayeredPane.DEFAULT_LAYER);

        digit0 = new JLabel("0");
        addListener(digit0,"0");
        digit0.setBounds(85, 300, 60, 60);
        digit0.setBorder(border);
        digit0.setForeground(Color.white);
        digit0.setFont(digit0.getFont().deriveFont(50f));
        digit0.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digit0, JLayeredPane.DEFAULT_LAYER);

        digitPlus = new JLabel("+");
        addListener(digitPlus,"+");
        digitPlus.setBounds(160, 300, 60, 60);
        digitPlus.setBorder(border);
        digitPlus.setForeground(Color.white);
        digitPlus.setFont(digit1.getFont().deriveFont(50f));
        digitPlus.setHorizontalAlignment(JLabel.CENTER);
        mainPlan.add(digitPlus, JLayeredPane.DEFAULT_LAYER);
    }

    private void addListener(JLabel label, String number) {
        label.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                smsText.setText(smsText.getText() + number);
            }

            @Override
            public void mousePressed(MouseEvent e) { }

            @Override
            public void mouseReleased(MouseEvent e) { }

            @Override
            public void mouseEntered(MouseEvent e) { }

            @Override
            public void mouseExited(MouseEvent e) { }
        });
    }
}
