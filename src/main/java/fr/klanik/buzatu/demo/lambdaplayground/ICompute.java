package fr.klanik.buzatu.demo.lambdaplayground;

import java.util.List;

@FunctionalInterface
public interface ICompute {
    Object calculateSomeThings(List<Integer> numbers);
}
