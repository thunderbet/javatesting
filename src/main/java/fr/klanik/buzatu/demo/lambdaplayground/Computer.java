package fr.klanik.buzatu.demo.lambdaplayground;

import java.util.Arrays;
import java.util.List;

public class Computer {

    /**
     * Method taking a list of integers and a functional interface doing something with that list
     *
     * @param numbers
     * @param computeInterface
     */
    public void calculate(List<Integer> numbers, ICompute computeInterface) {
        System.out.println(computeInterface.calculateSomeThings(numbers));
    }

    /**
     * Implementation of the sum of all elements of the list
     * @return
     */
    public static ICompute getAddFunction() {
        return (integers) -> {
            return integers.stream().mapToInt(Integer::intValue).sum();
        };
    }

    /**
     * Implementation of the average of all elements of the list
     * @return
     */
    public static ICompute getAverageFunction() {
        return (integers) -> {
            return integers.stream().mapToInt(Integer::intValue).average().getAsDouble();
        };
    }

    /**
     * Implementation just printing all elements of the list
     * @return
     */
    public static ICompute getStringFunction() {
        return (integers) -> {
            return "The list contains : " + integers;
        };
    }

    public static void main(String[] args) {
        Computer computerAdd = new Computer();
        List<Integer> listOfNumbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        computerAdd.calculate(listOfNumbers, getAddFunction());
        computerAdd.calculate(listOfNumbers, getAverageFunction());
        computerAdd.calculate(listOfNumbers, getStringFunction());
    }
}
