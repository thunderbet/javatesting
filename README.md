# JavaTesting project by Klanik

This project is a Java sandbox test project for learning and interview purposes.

#### CONTEXT

This project mimics sms tchating between several phones.  
You can send and display sms, with the condition that your phone is turn on.
Each phone has an independent GUI that can be handled.


#### INSTALLATION & REQUIREMENTS

To correctly run this project you will need the following :

- Your favourite IDE
- Java 8
- Maven 3+

Build the project with Maven, run the "PhoneTchat" class and your good to go.


#### EXERCISES

In the main class "PhoneTchat" you fill found different **cases** with something to do and some **questions**.

Example : 

```
/**
 *      Case 2 :
 *
 *      Use any of those play tests to do the following
 *
 *      - Power on just one phone and send the other phone 2 sms
 *      - Power on the second phone
 *
 *      Question :
 *
 *          => Why the second phone receives only the last sms ?
 *             Try to fix it.
 */
```

Your goal is to answer as many questions you can, the order is not important.
Then you should return the project with your modifications.

### **Good Luck**
